Game Design, Concept Development:
* All of Team Apogee

Credits:
Software Engineers:
* Alex Lowe
* Allan Davis
* Mike Churvis


UX designer, content design, level designer, sonification:
* Oriana “Jack” Ott


Visual Artist:
* Trae Dungy


Musician and Composition:
* Jesse James


-------------------------------

Link to google drive with all of our files:
https://drive.google.com/folderview?id=0Bx22a2soLCqKTVNDQU52SXJVd0E&usp=sharing

Repository link with all of the code and the game assets:
https://bitbucket.org/ggj_apogee/bigboots


-------------------------------
Credits:

Sounds:
Small car crash by Josh Mclelland
https://www.freesound.org/people/Northern_Monkey/sounds/185244/

https://www.facebook.com/JoshMclellandSoundDesigner
http://www.youtube.com/user/JoshJosh289/videos

Large crash by CGEeffects via freesound
*https://www.freesound.org/people/CGEffex/ pm with link to final project

safe effect, sewer sound by MovingPlaid
https://www.freesound.org/people/movingplaid/
quistard: body slam, water, bag
https://www.freesound.org/people/Quistard/
Richard Moore dinostomp, street ambience
https://www.freesound.org/people/richardemoore/



Images

http://upload.wikimedia.org/wikipedia/en/e/e1/PVA-epoxy_nanocomposite.JPG

http://opengameart.org/node/31671

http://opengameart.org/node/31714

http://chandra.harvard.edu/photo/2004/casa/casa_silicon_ions.jpg


(water)
http://images0.pixlis.com/background-image-circles-bubbles-sponge-soap-seamless-tileable-pale-green-link-water-23859a.png

http://opengameart.org/node/31698

http://commons.wikimedia.org/wiki/File:Towelblue.jpg

http://pixabay.com/en/towels-white-spa-beauty-clean-442835/
