﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Building))]
public class BuildingInspector : Editor
{
    public override void OnInspectorGUI()
    {
        

        if(GUILayout.Button("Build from sprites"))
        {
            Building building = (Building)target;

            int cc = building.transform.childCount;

            if (cc > 0)
            {
                for (int i = cc; i > 0; i--)
                {
                    DestroyImmediate(building.transform.GetChild(i-1).gameObject);
                }
            }

            building.Build();
        }


        DrawDefaultInspector();
    }
}
