﻿using UnityEngine;
using System.Collections;

public class PowerupController : MonoBehaviour, IInteractable
{
    public float MaxZScale { get { return maxZScale; } set { maxZScale = value; } }
    public float MinZScale { get { return minZScale; } set { minZScale = value; } }

    public float maxZScale;
    public float minZScale;
    public PlayerController player;

    private bool used = false;

    public string InteractAction(float ZScale)
    {
        //grow player
        if (gameObject == null)
        { return ""; }
        player.IncreaseSize();
        gameObject.GetComponent<AudioSource>().Play();
        used = true;
        gameObject.SetActive(false);

        return "Grow big";
    }

    public bool Invalid { get { return used; } }
    public GameObject getGameObject { get { return gameObject; } }
}