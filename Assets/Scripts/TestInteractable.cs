﻿using UnityEngine;
using System.Collections;

public class TestInteractable : MonoBehaviour, IInteractable
{
    public float MaxZScale { get { return maxZScale; } set { maxZScale = value; } }
    public float MinZScale { get { return minZScale; } set { minZScale = value; } }

    public float maxZScale;
    public float minZScale;
    public bool Invalid { get { return false; } }

    public string InteractAction(float ZScale)
    {
        switch (Mathf.RoundToInt(ZScale))
        {
            case 2:
                return "Level 2";
            case 3:
                return "Level 3";
            case 4:
                return "Level 4";
        }
        return "Failed to interact";
    }

    public GameObject getGameObject { get { return gameObject; } }
}
