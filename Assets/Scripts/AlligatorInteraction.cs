﻿using UnityEngine;
using System.Collections;


public class AlligatorInteraction : MonoBehaviour
{
    private Vector3 otherPosition;

    public Transform TargetTransform;

    public float AlligatorSpeed;

	private GameObject[] allSewers;
	private float startTime;

	void Awake(){
		allSewers = GameObject.FindGameObjectsWithTag("sewer");
	}

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.GetComponent<PlayerController>() != null)
        {
            otherPosition = other.transform.position;
        }


        if (other.GetComponent<PlayerController>().zScale <= -4f)
        {
            Flee();
        }
        else
        {
            Chase();
        }
    }


    

    private void Flee()
    {

        float journeyLength = Vector2.Distance(transform.position, TargetTransform.position);

        float distCovered = (Time.time - startTime) * (AlligatorSpeed * journeyLength);
        float fracJourney = distCovered / journeyLength;

        Camera.main.transform.position = new Vector3(0, 0, transform.position.z) + (Vector3)Vector2.Lerp(transform.position, TargetTransform.position, fracJourney);

        startTime = Time.time;
        transform.rotation.SetLookRotation(TargetTransform.position);
        transform.position = Vector3.Lerp(transform.position, TargetTransform.position, 1);
    }

    private void Chase()
    {
        
    }
}
