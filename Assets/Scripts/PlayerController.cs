﻿using UnityEngine;
using System.Collections;


[RequireComponent (typeof (Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    public float ZChange = -0.1f;
    public float playerScaleFactor = 0.1f;
    public float moveRateIncrease = 0.5f;
    public float moveRateDefault = 0.015f;

    public float zScale { get; private set; }

    private Rigidbody2D physicsBody;
    private float moveRate;
	private Animator anim;
    // Use this for initialization


    void Start()
    {
        physicsBody = this.GetComponent<Rigidbody2D>();
        zScale = 1;
        moveRate = moveRateDefault;
		anim = GetComponent<Animator>();
    }
	void Update(){
		//Debug things
		if (Input.GetAxisRaw("Size") == 1)
		{
			IncreaseSize();
		}
		if (Input.GetAxisRaw("Size") == -1)
		{
			
		}
	}
	
	
	// Update is called once per frame
    void FixedUpdate()
    {
        //Might want to make this not raw for mobile?
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
		Animating(horizontal, vertical);

        Vector2 movement = new Vector2(horizontal, vertical);
		physicsBody.MovePosition(physicsBody.position + (movement * moveRate));
		// rotate player

		float angle = Mathf.Atan2(vertical, horizontal) * Mathf.Rad2Deg;
		physicsBody.MoveRotation(angle);

		
    }

	void Animating(float h, float v){
		bool walking = h != 0f || v != 0f;
		anim.SetBool("IsWalking", walking);
	}
    public void IncreaseSize()
    {
        //move player
        Vector3 playerPos = transform.position;
        playerPos.z += -Mathf.Abs(ZChange);
        transform.position = playerPos;
        zScale = Mathf.Abs(playerPos.z);

		// play grow animation
		anim.SetTrigger("Grow");
        //scale up
        float newPlayerScale = transform.localScale.x + ZChange * playerScaleFactor;
        transform.localScale = new Vector3(newPlayerScale, newPlayerScale, newPlayerScale);

        //scale move speed appropriately
        moveRate += moveRateIncrease;

        Camera.main.GetComponent<CameraZoom>().ZoomToScale(Mathf.Abs(transform.position.z)); //camera zoom
        //play sounds?
    }

    public void ResetSize()
    {
        CameraZoom cameraZoom = Camera.main.GetComponent<CameraZoom>();
        Vector3 playerPos = this.gameObject.transform.position;

        //reset everything
        transform.position = new Vector3(playerPos.x, playerPos.y, cameraZoom.BaseSize);
        transform.localScale = new Vector3(cameraZoom.BaseSize, cameraZoom.BaseSize, cameraZoom.BaseSize);
        moveRate = moveRateDefault;
        cameraZoom.ZoomToScale(cameraZoom.BaseSize);
    }
}
