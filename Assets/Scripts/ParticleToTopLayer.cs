﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ParticleToTopLayer : MonoBehaviour
{
    public int SortingLayer = 1;


	// Use this for initialization
	void Start ()
	{
	    particleSystem.renderer.sortingLayerID = SortingLayer;
	}
}
