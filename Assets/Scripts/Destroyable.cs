﻿using UnityEngine;
using System.Collections;

public class Destroyable : MonoBehaviour
{
    public Animation transitionAnimation;
    public Sprite destroyedSprite;
    public float destructionZScale;
    public string destructionMessage = null;
    public AudioClip sound;

    public string DestroySelf(float ZScale)
    {
        if (destructionZScale > ZScale)
        { return null; }

        //sound
        gameObject.AddComponent<AudioSource>();
        AudioSource soundPlayer = gameObject.GetComponent<AudioSource>();
        soundPlayer.clip = sound;
        soundPlayer.Play();

        //animation


        //set final sprite
        gameObject.GetComponent<SpriteRenderer>().sprite = destroyedSprite;

        //remove physics
        Destroy(gameObject.rigidbody2D);
        Destroy(gameObject.collider2D);

        return destructionMessage;
    }
}
