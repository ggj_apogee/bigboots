﻿using UnityEngine;
using System.Collections;

public interface IInteractable
{
    float MinZScale { get; set; }
    float MaxZScale { get; set; }
    bool Invalid { get; }

    string InteractAction(float ZScale);
    GameObject getGameObject { get; }
}
