﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour {

    public Transform TargetTransform;
    public float Speed = 1f;

    Vector3 myPosition;

    float startTime;
    float journeyLength;
    float distCovered;
    float fracJourney;

    void Start()
    {
        startTime = Time.time;
    }

    void FixedUpdate()
    {

        myPosition = Camera.main.transform.position;
        journeyLength = Vector2.Distance(myPosition, TargetTransform.position);
        
        distCovered = (Time.time - startTime) * (Speed * journeyLength);
        fracJourney = distCovered / journeyLength;
		Debug.Log(new Vector3(0, 0, myPosition.z) + (Vector3) Vector2.Lerp(myPosition, TargetTransform.position, fracJourney));
        Camera.main.transform.position = new Vector3(0, 0, myPosition.z) + (Vector3) Vector2.Lerp(myPosition, TargetTransform.position, fracJourney);

        startTime = Time.time;
    }
}
