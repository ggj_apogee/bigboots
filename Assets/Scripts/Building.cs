﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class Building : MonoBehaviour 
{
	public List<GameObject> BuildingLayers = new List<GameObject>();

    public void Build()
    {
        for (int i = 0; i < BuildingLayers.Count; i++)
        {
            GameObject layer = (GameObject) Instantiate(BuildingLayers[(BuildingLayers.Count - 1) - i],
                transform.position + new Vector3(0, 0, GlobalVars.Z_SCALE_FACTOR),
                Quaternion.identity);

            layer.transform.parent = transform;

            foreach (SpriteRenderer sp in GetComponentsInChildren<SpriteRenderer>())
            {
                sp.sortingOrder += 10;
            }
        }
    }
}
