﻿using UnityEngine;
using System.Collections;

public class CameraZoom : MonoBehaviour
{
    public float BaseZ = -0.5f;
    public float BaseSize = 1f;

    public float ZScale = 1f;
    public float SizeScale = 1f;
    
    public void Start()
    {
        ZoomToScale(0);
    }

    public void ZoomToScale(float zoom)
    {
        Camera.main.orthographicSize = zoom*SizeScale + BaseSize;

        Vector3 pos = Camera.main.transform.position;

        Camera.main.transform.position = new Vector3(pos.x, pos.y, -(zoom * ZScale) + BaseZ);
    }
}
