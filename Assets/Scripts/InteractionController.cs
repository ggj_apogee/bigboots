﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


[RequireComponent(typeof(PlayerController), typeof(Rigidbody2D), typeof(Collider2D))]
public class InteractionController : MonoBehaviour
{
    private PlayerController player; 
    public Text messageBox = null;

    //holds the current object to interact with, 
    private IInteractable currentTarget = null;

    // Use this for initialization
    void Start()
    {
        player = GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxisRaw("Fire1") == 1)
        {
            if (currentTarget != null)
            {
                AttemptInteraction();
            }
            else
            {
                //Can't interact. display message, nothing nearby
                DisplayMessage("Cannot interact");
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D otherObj)
    {
        IInteractable interactable = (IInteractable)otherObj.gameObject.GetComponent(typeof(IInteractable));
        if (interactable != null)
        {
            currentTarget = interactable;
            //Change state of object to show it has been selected
            Debug.Log("Set object to " + otherObj.name);
        }
    }

    public void OnTriggerExit2D(Collider2D otherObj)
    {
        IInteractable interactable = (IInteractable)otherObj.gameObject.GetComponent(typeof(IInteractable));
        if (interactable != null && interactable.Equals(currentTarget))
        {
            currentTarget = null;
            //Change state of object to show it has been selected
            Debug.Log("removed object to " + otherObj.name);
        }
    }

    public void OnCollisionEnter2D(Collision2D collider)
    {
        Destroyable destroyable = collider.gameObject.GetComponent<Destroyable>();
        if (destroyable != null)
        {
            DisplayMessage(destroyable.DestroySelf(player.zScale));
        }
    }

    public void AttemptInteraction()
    {
        float ZScale = player.zScale;
        if (currentTarget.MinZScale > ZScale)
        {
            DisplayMessage("text box to smal");
        }
        if (currentTarget.MaxZScale < ZScale)
        {
            DisplayMessage("There's a snake in your boots!");
        }

        if (currentTarget.MaxZScale >= ZScale && currentTarget.MinZScale <= ZScale)
        {
            Debug.Log("Interacted, Z: " + ZScale + " Obj: " + currentTarget.getGameObject.name);
            DisplayMessage(currentTarget.InteractAction(ZScale));
            if (currentTarget.Invalid)
            { currentTarget = null; }
        }
    }

    public void DisplayMessage(string message)
    {
		if (message == null){
			messageBox.text = "";
		}else if (!message.Equals("")){
            messageBox.text = message;
        }
        
        //Automatically remove messages using async things?

        //Prevent messages from changing too fast?
    }

}
